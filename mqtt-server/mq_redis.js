require('sys');
var redis = require('redis');

var method = mq_redis.prototype;

function mq_redis(hosts, logger)
{
  this.def_port 	= 6379;
  this.servers		= [];
  this.server_num 	= 0;
  this.hosts 		= hosts;
  this.logger 		= logger;

  this._connect();
}

method._connect = function()
{
  if (this.server_num > 0)
    return true;

  for (var i = 0; i < this.hosts.length; i++) {
    var port = this.def_redis_port;
    if (!this.hosts[i].host)
      continue;
    if (this.hosts[i].port)
      port = this.hosts[i].port;

    try {
      this.servers[i] = redis.createClient(port, this.hosts[i].host, {});
    } catch (e) {
      console.log("Redis Error: " + e);
      /*
      this.logger.warn('Reids connect error: '+ err);
      */
    }

    if (!this.servers[i])
      continue;

    this.server_num++;
  }

};

method.set = function (k, v)
{
  if (this.server_num < 0)
    return false;

  this.servers[0].set(k, v);
};

method.incr = function (k)
{
  if (this.server_num < 0)
    return false;

  this.servers[0].incr(k, v);
};

method.hlen = function (k, cb)
{
  if (this.server_num < 0)
    return false;

  this.servers[0].hlen(k, function (err, reply) {
    if (err) {
      console.log('Hlen error: ' + err);
      return cb(0);
    }

    return cb(reply);
  });
};

method.hmget = function (n, keys, cb)
{
  if (this.server_num < 0)
    return false;

  this.servers[0].hmget(n, keys, function (err, reply) {
    if (err) {
      console.log('Hmget error: ' + err);
      return cb(null);
    }

    return cb(reply);
  });

};

method.hset = function (n, k, v)
{
  if (this.server_num < 0)
    return false;

  this.servers[0].hset(n, k, v);
};

method.hincrby = function (n, k, v)
{
  if (this.server_num < 0)
    return false;

  this.servers[0].hincrby(n, k, v);
};

method.discard = function ()
{
  if (this.server_num < 0)
    return false;

  this.servers[0].discard();
};

method.hget = function (n, k, cb)
{
  if (this.server_num < 0) {
    return cb(false);
  }
  if (this.server_num == 1) {
    this.servers[0].hget(n, k, function(err, reply) {
      console.log('mq_redis ' + reply);
      return cb(reply);
    });
  }

};

method.hgetall = function (n, cb)
{
  if (this.server_num < 0)
    cb(false);
  if (this.server_num == 1) {
    return this.servers[0].hgetall(n, function(err, reply) {
      cb(reply);
    });
  }
};

method.hincryby = function (n, k, v, cb)
{
   if (this.server_num < 0)
    cb(false);
  if (this.server_num == 1) {
    return this.servers[0].hincryby(n, k, v, function(err, reply) {
      cb(reply);
    });
  }
};


method.hdel = function (n, k)
{
  if (this.server_num < 0)
    return false;
  if (this.server_num == 1) {
    return this.servers[0].hdel(n, k);
  }
};

method.zadd = function (args, cb)
{
  if (this.server_num < 0)
	cb(false);

  if (this.server_num == 1) {
    try {
      this.servers[0].zadd(args, function (err, reply) {
	if (cb) {
	  cb(err, reply);
	}
      });
    } catch (e) {
      console.log(e);
    }

  console.log(this.server_num);
  }
};

method.zrevrange = function(args, cb)
{
  if (this.server_num < 0)
    return false;

  if (this.server_num == 1) {
    return this.servers[0].zrevrange(args, function (err, reply) {
      if (cb) {
	cb(err, reply);
      }
    });
  }

};

method.zrange = function (args, cb)
{
  if (this.server_num < 0)
    return false;

  if (this.server_num == 1) {
    return this.servers[0].zrange(args, function (err, reply) {
      if (cb) {
	return cb(err, reply);
      }
    });
  }
};

method.zcard = function (k, cb)
{
  if (this.server_num < 0)
    return false;

  var _logger = this.logger;
  if (this.server_num == 1) {
    return this.servers[0].zcard(k, function (err, reply) {
      if (cb) {
	return cb(err, reply);
      }
      console.log(err);
      _logger.warn(err);
    });
  }
};

method.multi = function ()
{
  if (this.server_num < 0)
    return false;

  var _logger = this.logger;
  if (this.server_num == 1) {
    return this.servers[0].multi();
  }
};

method.sadd = function (k, v)
{
  if (this.server_num < 0)
    return false;

  var _logger = this.logger;
  if (this.server_num == 1) {
    return this.servers[0].sadd(k, v);
  }
};

method.watch = function (k)
{
  if (this.server_num < 0)
    return false;

  var _logger = this.logger;
  if (this.server_num == 1) {
    return this.servers[0].watch(k);
  }

};

method.zremrangeall = function (k, cb)
{
  if (this.server_num < 0)
    return false;

  var _logger = this.logger;
  if (this.server_num == 1) {
    return this.servers[0].zremrangebyrank([k, 0, -1], function (err, reply) {
      if (cb) return cb(err, reply);
    });
  }

};

method.get = function (k, cb)
{
  if (this.server_num < 0)
    cb(false);

  if (this.server_num == 1) {
    return this.servers[0].get(k, function(err, reply) {
      cb(reply);
    });
  }
};

method.quit = function ()
{
  for (i = 0; i < this.server_num; i++) {
    this.servers[i].quit();
  }
};

module.exports = mq_redis;

/*
   var logger = require('./log.js')();

   mr = new mq_redis([
   {host: "localhost", port: 6379, master: 1}],
   logger);

   mr.set('a', 'asdfasdfdsaf');
   mr.set('b', 'dfdfdfdfdf');
   mr.get('a', function (r) {
   console.log(r);
   });
   mr.quit();
   */
