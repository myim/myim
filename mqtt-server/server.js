var sys 	= require('sys');
var net 	= require('net');
var mqtt 	= require('mqtt');
var mq_redis  	= require('./mq_redis.js');
var mq_log 	= require('./log.js');
var mq_config 	= require('./config.js');
var crypto 	= require('crypto'); 
var util 	= require('util');
var err_code 	= require('./err_code.js')();
var mq_msg	= require('./msg.js');
var siojwt	= require('socketio-jwt');
var mq_auth	= require('./auth.js');
var mysql	= require('mysql');
var jwt 	= require('jsonwebtoken');


try {
  var config 	= new mq_config().get();
  var logger	= new mq_log();
  var my_redis 	= new mq_redis(config.redis, logger);
  var msg 	= new mq_msg(false, my_redis, logger, log_error);
  var auth	= new mq_auth(config, mysql, logger, jwt, false);
} catch(e) {
  console.log('err: ' +e);
  process.exit(1);
}

process.on('uncaughtException', function (err) { 
  console.log('Caught exception: ' + err); 
  logger.warn('Caught exception: ' + err);
});

/*
 * Main server
 *
 * require:
 * 	opt.config
 * 	opt.redis
 * 	opt.logger
 * 	opt.msg
 */
mq_server = function (opt)
{
  var self 	= this;
  self._config 	= opt._config;
  self._redis 	= opt._redis;
  self._logger	= opt._logger;
  self._msg	= opt._msg;
  self._auth	= opt._auth;
  self._mqtt	= opt._mqtt;
  self._port	= self._config.server.listen ? self._config.server.listen : 5000;

  /* Namespace of public */
  self.pub_ns	= '/public';
  /* Namespace of chat */
  self.chat_ns	= '/chat';

  self.REDIS_CLIENTS_KEY = '__CLIENTS';

  /* MQTT client pool */
  self._clients = {};

  /* socket.io */
  self.io 	= null;

  self.init();
};

var _method = mq_server.prototype;

/*
 * Init io and setup authorization
 */
_method.init = function ()
{
  var self = this;

  self.MQ_MSG_STAT_SENT    	= 1;
  self.MQ_MSG_STAT_READED  	= 2;

  try {
    self.io  	= require('socket.io')(
	{'transports' : [
	  'websocket',
      'flashsocket',
      'htmlfile',
      'xhr-polling',
      'jsonp-polling'
      ]}).listen(self._port);
  } catch (e) {
    console.log('IO listen failed: ' + e + e.stack);
    self._logger.warn('IO listen failed: ' + e);
  }

  console.log("Create server at: " + self._port);

  
  self.io.of(self.pub_ns).on('connection', function (socket) {
    self._handle_pub_connection(socket);
  });

  /* Protecting chat namespace with socketioJwt token */
//  console.log('secret: ' + self._auth.secret);
  

  //self.io.use(siojwt.authorize({
  self.io.of(self.chat_ns).use(siojwt.authorize({
    secret: self._auth.secret,
  handshake: true
  }));

  //self.io.on('connection', function(socket) {
  self.io.of(self.chat_ns).on('connection', function(socket) {
    socket.namespace = self.chat_ns;

    self._handle_chat_connection(socket);
  });

};

/*
 * Public namespace connection handler
 */
_method._handle_pub_connection = function(socket)
{
  var self = this;

  socket.on('login', function (data) {
    console.log('In login event');
    if (!data || !data.uname || !data.passwd) {
      console.log('Login failed, no login data');
      return log_error('Login failed, no login data');
    }
    self._auth.backend.login(data.uname, data.passwd, function (ret) {
      if (!ret) {
	return false;
      }

      var room = self.get_user_room_name(ret.orig.userid);

      socket.join(room);

      console.log('Logged in: token = '+ ret.token);
      self.io.of(self.pub_ns).to(room).emit(
	'logged',
	{user_id: ret.orig.userid, token: ret.token});
    });
  });

};

/*
 * Create mqtt client
 */
_method.create_mqtt_client = function(user_id)
{
  var self = this;
  var client 	= null;

  if (!self._config.mqtt_servers[0]) {
    console.log('Please configure mqtt servers');
    return false;
  }

  var c 	= self._config.mqtt_servers[0];
  try {
    client = self._mqtt.createClient(
	c.port ? c.port : 1883,
	c.host ? c.host : '127.0.0.1',
	{
	  clientId: String(user_id),
	   encoding: 'utf8',
	   keepalive: 10,
	   clean:true
	});
  } catch (e) {
    console.log(e);
    self._logger.warn(e);
    return false;
  }

  return client;
};

/*
 * Get mqtt client by user_id
 */
_method.get_mqtt_client = function(user_id)
{
  var self = this;
  if (self._clients && self._clients[user_id])
    return self._clients[user_id];
  return null;
};

/*
 * Store user mqtt client
 */
_method.save_mqtt_client = function(user_id, client)
{
  var self = this;
  if (!self._clients || !self._clients[user_id])
    self._clients[user_id] = client;
};

/*
 * Get user room name
 */
_method.get_user_room_name = function(user_id)
{
  return '/U_' + user_id;
};

/*
 * Handle mqtt message event
 */
_method.handle_mqtt_message = function (client, user_id)
{
  var self = this;

  console.log('in messsage');
  client.on('message', function(topic, message, packet) {
    console.log('** Message event: ' + topic + ' '  + user_id +':'+ self.MQ_MSG_STAT_SENT + message);
    //    self._msg.process_msg(user_id, topic, message, self.MQ_MSG_STAT_SENT, function (data)

    var _ta = topic.split('/');

    console.log('in mqtt message _ta: ' + _ta  + packet.messageId);
    if (!_ta)
      return false;

    if (_ta[0] == '_S') {
      if (!_ta[1] || !_ta[2]) {
        return false;
      }
    }

    if (!packet) {
      return  false;
    }
    self.io.of(self.chat_ns).to(self.get_user_room_name(user_id)).emit(
                                                                       'mqtt', {
                                                                         'from_id': _ta[2],
      'msg_id': packet.messageId,
      'topic': String(topic), 
      'payload': String(message),
      'timestamp':  Date.parse(new Date()) 
                                                                       }
                                                                      );
  });

};

/*
 * Handle get my unread msg notify event
 */

_method.handle_get_my_unread_notify = function (socket, user_id)
{
  var self = this;
  socket.on('get_my_unread_notify', function () {
    console.log('In unread_notify');
    try {
      self._msg.get_my_unread_notify(user_id, function (reply) {
        self.io.of(self.chat_ns).to(self.get_user_room_name(user_id)).emit('get_my_unread_notify_ok', reply);
      });
    } catch (e) {
      console.log(e);
    }
  });
};

/*
 * Handle get my unread msg notify event
 */
_method.handle_get_my_sended_notify = function (socket, user_id)
{
  var self = this;
  socket.on('get_my_sended_notify', function () {
    console.log('In sended_notify');
    try {
      self._msg.get_my_sended_notify(user_id, function (reply) {
        self.io.of(self.chat_ns).to(self.get_user_room_name(user_id)).emit('get_my_sended_notify_ok', reply);
      });
    } catch (e) {
      console.log(e);
    }
  });
};

/*
 * Handle messaging stat event
 */
_method.handle_messaging = function (socket, user_id)
{
  var self = this;

  socket.on('set_readed', function (data) {
    console.log('In messaging handle');
    if (!data) {
      return false;
    }
    self._msg.set_readed(user_id, from_id, function (reply) {
      self.io.of(self.chat_ns).to(self.get_user_room_name(user_id)).emit('set_readed_ok', reply);
    });
  });
};

/*
 * Handle get buddys
 */
_method.handle_get_buddys = function (socket, user_id)
{
  var self = this;

  socket.on('get_kf_buddys', function() {
    try {
      self._auth.backend.get_kf_buddys(user_id, function (reply) {
        console.log('In get kf buddys');
        if (reply) {
          self.io.of(self.chat_ns).to(self.get_user_room_name(user_id)).emit('get_buddys_ok', reply);
          //self.io.sockets.emit('get_buddys_ok', reply);
        }
      });
    } catch (e) {
      console.log('get_budds e');
    }
  });

  socket.on('get_normal_buddys', function() {
    try {
      self._auth.backend.get_normal_buddys(user_id, function (reply) {
        console.log('In get normal buddys');
        if (reply) {
          //	console.log(reply);
          self.io.of(self.chat_ns).to(self.get_user_room_name(user_id)).emit('get_buddys_ok', reply);
          //self.io.sockets.emit('get_buddys_ok', reply);
        }
      });
    } catch (e) {
      console.log('get_budds e');
    }
  });
};

/*
 * Handle publish message event
 */
_method.handle_publish = function (socket, client, user_id)
{
  var self = this;

  socket.on('publish', function (data) {
    if (!data || !data.to_id || !data.topic || !data.msg) {
      console.log('data message is none');
      return false;
    }

    console.log('Publishing to topic: ' + data.topic + ', id: ' + data.to_id);
    data.user_id = user_id;

    try {
      self._msg.process_msg(user_id, data.topic, data.msg, self.MQ_MSG_STAT_SENT, function (reply) {
        if (!reply) {
          console.log('!!proc msg error');
          return false;
        }
        console.log('** msg_id: ' + reply.msg_id);
        client.publish(data.topic, data.msg, {qos: 1, retain:false, messageId: reply.msg_id}, function (err, granted) {
          if (err) {
            console.log(err);
            logger.war(err);
            return self.io.of(self.chat_ns).to(self.get_user_room_name(user_id)).emit('publish_fail');
          }
          console.log('publish ok');
          return self.io.of(self.chat_ns).to(self.get_user_room_name(user_id)).emit('publish_ok', {from_id: user_id, msg:data.msg});
        });
      });
    } catch (e) {
      console.log(e);
      logger.warn(e);
      return io.of(self.chat_ns).to(self.get_user_room_name(user_id)).emit('publish_fail');
      //io.sockets.in(data.topic).emit(0);
    }
  });
};

/*
 * Handle subscribe event
 */
_method.handle_subscribe = function (socket, client, user_id)
{
  var self = this;

  console.log('in subscribe');
  socket.on('subscribe', function (data) {
    if (!data) {
      console.log("Subscribing failed: no data");
      return false;
    }

    console.log('Subscribing to ' + data);

    try {
      client.subscribe(data, {qos: 1, dup: true}, function (err, granted) {
        if (err) {
          console.log(err);
          logger.war(err);
        }
        self.io.of(self.chat_ns).to(self.get_user_room_name(user_id)).emit('subscribe_ok', granted);
      });
    } catch (e) {
      console.log(e);
      logger.warn(e);
      //io.sockets.in(data.topic).emit(0);
    }
  });
};

/*
 * Caht room (authorized users) connection handler
 */
_method._handle_chat_connection = function(socket) 
{
  var self = this;
  console.log(socket.decoded_token);
  var user_id = socket.decoded_token.userid;

  console.log(user_id, 'connected');
  var room = self.get_user_room_name(user_id);

  console.log('joining room:' + room);
  socket.join(room);

  var client = null;
  if (!(client = self.get_mqtt_client(user_id)) && 
      !(client = self.create_mqtt_client(user_id))) {
        console.log('mqtt client create failed!');
        return false;
      }

  self.handle_get_buddys(socket, user_id);
  self.handle_get_my_unread_notify(socket, user_id);
  self.handle_get_my_sended_notify(socket, user_id);
  self.handle_messaging(socket, user_id);
  self.save_mqtt_client(user_id, client);
  self.handle_mqtt_message(client, user_id);
  self.handle_publish(socket, client, user_id);
  self.handle_subscribe(socket, client, user_id);
};

_method._get_client = function (client_id)
{
  var self = this;

  if (self._clients && self._clients[client_id]) {
    return self._clients[client_id];
  } 

  return false;
};

function log_error(err, reply)
{
  if (err) {
    console.log(err);
    logger.warn(err);
  }
}

try {
  var server = new mq_server(
                             { 
                               _siojwt: siojwt,
      _config : config,
      _redis: my_redis,
      _msg: msg,
      _logger: logger,
      _auth: auth,
      _mqtt: mqtt
                             });
} catch (e) {
  console.log(e);
  process.exit(2);
}

