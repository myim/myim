require('sys');
var fs 	= require('fs');
var yaml= require('js-yaml');

var method = mq_config.prototype;

function mq_config(file)
{
  this.yaml_config_file = './etc/global.yml';
  if (file)
    this.yaml_config_file = file;
  
  this.doc = {};

  try {
    this.doc = yaml.safeLoad(fs.readFileSync(this.yaml_config_file, 'utf8'));
  } catch (e) {
    console.log(e);
  }
}

method.get = function(k) 
{
  if (!k)
    return this.doc;
};

module.exports = mq_config;

/*
var config = new mq_config();
console.log(config.get());
*/
