function navList(id) {
  var $obj = $("#nav_dot"), $item = $("#J_nav_" + id);
  $item.addClass("on").parent().removeClass("none").parent().addClass("selected");
  $obj.find("h4").hover(function () {
    $(this).addClass("hover");
  }, function () {
    $(this).removeClass("hover");
  });
  $obj.find("p").hover(function () {
    if ($(this).hasClass("on")) { return; }
    $(this).addClass("hover");
  }, function () {
    if ($(this).hasClass("on")) { return; }
    $(this).removeClass("hover");
  });
  $obj.find("h4").click(function () {
    var $div = $(this).siblings(".list-item");
    if ($(this).parent().hasClass("selected")) {
      $div.slideUp(600);
      $(this).parent().removeClass("selected");
    }
    if ($div.is(":hidden")) {
      $("#nav_dot li").find(".list-item").slideUp(600);
      $("#nav_dot li").removeClass("selected");
      $(this).parent().addClass("selected");
      $div.slideDown(600);

    } else {
      $div.slideUp(600);
    }
  });
}

io_mqtt = function(user_id, token, def_topics, host, port)
{
  this.host 		= host ? host : '127.0.0.1';
  this.port 		= port ? port : '5000';
  this.user_id		= user_id ? user_id : 0;
  this.token		= token ? token : '';

  this.socket		= null;
  this.def_topics 	= def_topics ? def_topics : null;

  this.inited 		= false;

  this.proto 		= {
    init: "init",
    recv: "subscribe",
    send: "publish",
    close: "disconnect"
  };

  this.set_topics();
};

_io_mqtt_method = io_mqtt.prototype;

_io_mqtt_method.set_topics = function()
{
  if (!this.def_topics) {
    this.def_topics = [
    "_S." + this.user_id + '.*', 
    "__boardcast"];
  }
};

_io_mqtt_method.connect = function(ns, transport, token)
{
  var _t = token ? token : this.token;
  if (Modernizr.websockets) {
    if (_t) {
      this.socket 	 = io.connect('http://' + this.host + ':' + this.port +
	  (ns ? ns : ''), {'query': 'token=' + _t, transports:['websocket']});
    } else {
      this.socket 	 = io.connect('http://' + this.host + ':' + this.port +
	  (ns ? ns : ''), {transports:['websocket']});
    }
  } else {
    if (_t) {
      this.socket 	 = io.connect('http://' + this.host + ':' + this.port +
	  (ns ? ns : ''), {'query': 'token=' + _t});
    } else {
      this.socket 	 = io.connect('http://' + this.host + ':' + this.port +
	  (ns ? ns : '')
	  );
    }
  }

  return this.socket;
};

_io_mqtt_method.on_connect = function(socket, cb)
{
  socket.on('connect', function(socket) {
    cb();
  });
};

_io_mqtt_method.init = function (cb)
{
  this.socket.emit('init', {client_id: this.client_id});
};

_io_mqtt_method.on_init = function (cb)
{
  this.socket.on('init_ok', function (data) {
    cb(data);
  });
};

_io_mqtt_method.def_subcribe = function (socket, cb)
{
  if (this.def_topics) {
    socket.emit('subscribe', this.def_topics);
  }
};

_io_mqtt_method.get_buddys = function(socket, cb)
{
  socket.emit('get_buddys');
  socket.on('get_buddys_ok', function(data) {
    cb(data);
  });
};

var _interval_pool = [];
var _msg_pool = {};

function bling(id, obj, interval)
{
  var i = 0;
  
  if (_interval_pool[id]) {
    return;
    window.clearInterval(_interval_pool[id]);
    _interval_pool[id] = 0;
    return;
  }

  _interval_pool[id] = window.setInterval(function(){  
    i++;
    if (i % 2 === 0) {
      obj.css("margin-top", "1px");
      obj.css("margin-left", "1px");
    } else {
      obj.css("margin-top", "0px");
      obj.css("margin-left", "-1px");
    }
    if (i == 1024) {
      i = 0;
    }
  },interval ? interval: 300);  
  return _interval_pool[id];
}

function clear_bling(id)
{
  if (_interval_pool && _interval_pool[id]) {
    _interval_pool[id] = window.clearInterval(_interval_pool[id]);
  }
}

function save_msg(id, data)
{
  if (!_msg_pool[id])
    _msg_pool[id] = new Array();

  _msg_pool[id][_msg_pool[id].length] = data;
}

function get_msg(id)
{
  return _msg_pool[id] ? _msg_pool[id] : false;
}

function clear_msg(id)
{
  if (_msg_pool[id]) {
    _msg_pool[id] = null;
  }
}
