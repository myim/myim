require('sys');
var mq_log 	= require('./log.js');
var mq_config 	= require('./config.js');
var request	= require('request');
var crypto 	= require('crypto');
var mysql      	= require('mysql');

var method 	= mq_auth.prototype;

function mq_auth(config, mysql, logger, jwt, auth_url)
{
  var self = this;

  self.kf_group_id = 11;

  this.auth_url = auth_url ? auth_url : 'http://www.xiehui.com/api/ci/app/get_zhuzhi';
  this.app_code = crypto.createHash('md5').update('xpzl').digest('hex');

  self.logger 	= logger ? logger : null;
  self.config 	= config ? config : null;
  self.jwt	= jwt ? jwt : null;

  if (!self.logger || !self.config || !self.jwt) {
    console.log('auth lib missed');
    return false;
  }

  self.secret 	= self.config.secret.key ? 
    self.config.secret.key : '$1$zspGAdkX$hMRnGPWi4ZSKEXGv.xAHt/';
  self.token_expire = self.config.auth.token_expire ? 
    self.config.auth.token_expire : 12 * 60;
  console.log(self.token_expire);

  self.c 	= config;
  self.mysql 	= mysql ? mysql : null;
  self.backend 	= null;

  var b = self.c.auth.backend ? self.c.auth.backend : 'mysql';

  switch (b) {
    default: 
      self.backend = new auth_backend_mysql(self.mysql, self.c, self.jwt, self);
  }

}

var auth_backend_mysql = function (mysql, config, jwt, parent)
{
  var self    = this;
  self.c      = config;
  self.mysql  = mysql;
  self.jwt    = jwt;

  self.con    = null;

  self.login = function(uname, pass, cb)
  {
    self.connect(function(con) {
      if (!uname || !pass) {
	return cb(false);
      }

      try {
//	key = pass + (self.c.auth.auth_key ? self.c.auth.auth_key : '');
	hash = self.hash_pass(pass);
        console.log(hash);
	con.query(
	  'SELECT truename, userid, groupid, company AS C FROM '+ self.c.auth.dsn.db_name + 
	  '.destoon_member WHERE username = ? AND password = ? LIMIT 1', 
	  [uname, hash], function (err, rows) {
	    if (err || !rows.length) {
	      if (err) {
		console.log(err);
	      }
	      self.con.destroy();
	      return cb(false);
	    }
	    self.con.destroy();
	    return cb({token: self.gen_token(rows[0]), orig: rows[0]});
	  });
      } catch (e) {
	self.con.destroy();
	return cb(false);
      }
    });
  };

  self.gen_token = function (profile)
  {
    var token = '';
    console.log(parent.secret);
    console.log(parent.token_expire);
    try {
      token = self.jwt.sign(profile, parent.secret, { expiresInMinutes: parent.token_expire });
    }catch (e) {
      console.log(e);
    }

    return token;
  };


  self.hash_pass = function(pass)
  {
    key = crypto.createHash('md5').update(pass).digest('hex');
    return crypto.createHash('md5').update(key).digest('hex');
  };

  self.get_arch = function()
  {
    self.connect(function() {
    });
  };

  self.get_kf_buddys  = function (user_id, cb)
  {
    self.connect(function (con) {
      try {
	con.query(
	  "SELECT userid, groupid, truename, username FROM " + self.c.auth.dsn.db_name+ 
	  ".destoon_member WHERE `groupid` = 1 AND kefu = 1",
	  function (err, rows) {
	    if (err || !rows.length) {
	      if (err) {
		console.log(err);
	      }
	      self.con.destroy();
	      return cb(false);
	    }
	    self.con.destroy();
	    return cb(rows);
	  });
      } catch (e) {
	self.con.destroy();
	return cb(false);
      }
    });
  };

  self.get_normal_buddys  = function (user_id, cb)
  {
    self.connect(function (con) {
      try {
	con.query(
	  "SELECT userid, groupid, truename, username FROM " + self.c.auth.dsn.db_name+ 
	  ".destoon_member WHERE `groupid` <> 1 AND kefu <> 1",
	  function (err, rows) {
	    if (err || !rows.length) {
	      if (err) {
		console.log(err);
	      }
	      self.con.destroy();
	      return cb(false);
	    }
	    self.con.destroy();
	    return cb(rows);
	  });
      } catch (e) {
	self.con.destroy();
	return cb(false);
      }
    });
  };

  self.disconnect = function ()
  {
  };

  self.connect = function(cb)
  {
    try {
      self.con = self.mysql.createConnection({
	host     : self.c.auth.dsn.host ? self.c.auth.dsn.host : '127.0.0.1',
	port     : self.c.auth.dsn.port ? self.c.auth.dsn.port : 3306,
	user     : self.c.auth.dsn.user ? self.c.auth.dsn.user : 'root',
	password : self.c.auth.dsn.pass ? self.c.auth.dsn.pass : 'root',
      });
    } catch (e) {
      return console.log(e);
    }

    self.con.connect(function(err) {
      if (err) {
	console.error('error connecting: ' + err.stack);
	return false;
      }
      return cb(self.con);
    });
  };

};


method.get_perms = function (sessid, user_name, cb)
{
  if (!sessid || !user_name)
    cb(false);

  request.post(this.auth_url, {form:{sessionid:sessid, username: user_name, app_code:this.app_code}}, function (e, r, body) {
    if (!e && r.statusCode == 200) {
      cb(body);
    } else {
      console.log(e);
    }
  });
};

module.exports = mq_auth;

/*
var jwt = 	require('jsonwebtoken');

try {
  var mq_auth = new mq_auth(
      new mq_config(),
      mysql,
      new mq_log(),
      jwt,
      false
      );

} catch (e) {
  console.log(e);
}

mq_auth.backend.login('hoho', '123456', function (ret) {
  console.log(ret);
});
*/
