require('sys');
var jwt 	= require('jsonwebtoken');
var express 	= require('express');
var mq_log 	= require('./log.js');
var mq_config 	= require('./config.js');
var crypto 	= require('crypto');
var mysql      	= require('mysql');
var mq_auth	= require('./auth.js');
var bodyParser 	= require('body-parser')

var app 	= express();
var listen	= 8080;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

process.on('uncaughtException', function (err) { 
  console.log('Caught exception: ' + err); 
});

try {
  var mq_auth = new mq_auth(
      new mq_config().get(),
      mysql,
      new mq_log(),
      jwt,
      false
      );

} catch (e) {
  console.log(e);
}

app.all('*',function(req,res,next){
   res.set({
       'Access-Control-Allow-origin' : '*',
       'Access-Control-Allow-Headers' : 'X-Requested-With',
       'Access-Control-Allow-Methods' : 'GET,POST'
   }) ;
    next();
});

app.post('/login', function (req, res) {
  if (!req.body || !req.body.uname || !req.body.passwd) {
    console.log('empty login data');
    return res.send({token: ''});
  }
  
  console.log(req.body.uname  + req.body.passwd);
  mq_auth.backend.login(req.body.uname, req.body.passwd, function (ret) {
    try {
      if (!ret) {
	console.log('login failed');
	//return res.json({token: ''});
	res.send('failed');
      } else {
	res.send({user_id: ret.orig.userid, group_id: ret.orig.groupid, token: ret.token});
      }
    } catch (e) {
      console.log(e);
      res.send({token: ''});
    }
  });
});

app.listen(8080);
