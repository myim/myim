var sys 	= require ('sys');
var crypto 	= require('crypto'); 
var lazy	= require('lazy');

method 		= mq_msg.prototype;

var REDIS_MSG_BUDDYS	= "__MSG_BUDDYS";
var REDIS_MSG_USER_BUDDYS	= "__MSG_USER_BUDDYS";
var REDIS_MSG_POOL 	= "__MSG_POOL";
var MQ_MSG_COUNTER	= "__MSG_COUNTER";
var MQ_MSG_UNREAD_TO_COUNTER = "__MSG_STAT_POOL_TO";
var MQ_MSG_UNREAD_FROM_COUNTER   = "__MSG_STAT_POOL_FROM";
var MQ_MSG_STAT_INDEX = "__MSG_STAT_INDEX";
var MQ_MSG_TIME_INDEX = "__MSG_TIME_INDEX";


var MQ_MSG_BUDDY_STAT_OFFLINE 	= 0;
var MQ_MSG_BUDDY_STAT_ONLINE 	= 1;
var MQ_MSG_BUDDY_STAT_BUSY 	= 2;
var MQ_MSG_BUDDY_STAT_LEFT 	= 3;
var MQ_MSG_BUDDY_STAT_TYPING 	= 4;


var _mq_msg_buddys = [
  { nick_name: 'bbbb', client_id: 2, level: 1, stat: MQ_MSG_BUDDY_STAT_OFFLINE, parent: 0, dep: '财务', desc:'好啊'},
  { nick_name: 'cccc', client_id: 3, level: 2, stat: MQ_MSG_BUDDY_STAT_LEFT, parent: 1, dep: '出纳', desc:'好啊c'},
  { nick_name: 'dddd', client_id: 4, level: 2, stat: MQ_MSG_BUDDY_STAT_ONLINE, parent: 1, dep: '出纳', desc:'好啊d'},
  { nick_name: 'eeee', client_id: 5, level: 2, stat: MQ_MSG_BUDDY_STAT_TYPING, parent: 1, dep: '出纳', desc:'好啊e'},
  { nick_name: 'fff', client_id: 6, level: 2, stat: MQ_MSG_BUDDY_STAT_BUSY, parent: 1, dep: '出纳', desc:'好啊f'},
];

var _mq_msg_user_buddys = [
  { nick_name: 'aaaa', client_id: 1, level: 1, stat: MQ_MSG_BUDDY_STAT_OFFLINE, parent: 0, dep: '财务', desc:'没什么说的'},
  { nick_name: 'cccc', client_id: 201, level: 1, stat: MQ_MSG_BUDDY_STAT_OFFLINE, parent: 0, dep: '财务', desc:'好啊'},
  { nick_name: '444', client_id: 301, level: 2, stat: MQ_MSG_BUDDY_STAT_LEFT, parent: 1, dep: '出纳', desc:'好啊c'}
];

function mq_msg(backend, redis, logger, cb)
{
  this.backend 	= backend 	? backend 	: {driver: 'internal'};
  this.redis 	= redis 	? redis 	: null;
  this.logger	= logger 	? logger 	: null;
  this.multi	= null;
  this.MQ_MSG_STAT_SENT    	= 1;
  this.MQ_MSG_STAT_READED  	= 2;

  if (!this.logger) {
  }

  if (this.backend.driver == 'internal') {
    if (!_mq_msg_buddys) {
      cb(false);
    }

    redis.zremrangeall(REDIS_MSG_BUDDYS, function (err, reply) {
      _mbl 	= _mq_msg_buddys.length;
      _args	= [REDIS_MSG_BUDDYS];

      for (var i = 0; i < _mbl; i++) {
        _args[2*i+1] = _mq_msg_buddys[i].level;
        _args[2*i+2] = JSON.stringify(_mq_msg_buddys[i]);
      }


      try {
        redis.zadd(_args, function (err, reply) {
          cb(err, reply);
        });
      } catch(e) {
        console.log(e);
      }
    });

    redis.zremrangeall(REDIS_MSG_USER_BUDDYS, function (err, reply) {
      _mbl 	= _mq_msg_user_buddys.length;
      _args	= [REDIS_MSG_USER_BUDDYS];

      for (var i = 0; i < _mbl; i++) {
        _args[2*i+1] = _mq_msg_user_buddys[i].level;
        _args[2*i+2] = JSON.stringify(_mq_msg_user_buddys[i]);
      }

      try {
        redis.zadd(_args, function (err, reply) {
          cb(err, reply);
        });
      } catch(e) {
        console.log(e);
      }
    });

  }
}

method.get_htime = function()
{
  var diff = process.hrtime();
  return (diff[0] * 1e9 + diff[1]) / 1e6;
};

method.get_msg_pool_key = function (from_id, to_id, timestamp)
{
  return (REDIS_MSG_POOL + '_' + from_id + '_' + to_id);
};

method.get_buddys = function (client_id, limit, cb)
{
  var _client_id  = client_id ? client_id : 1;
  var _limit 	    = limit ? limit : -1;
  var _args 	    = [client_id != 1 ? REDIS_MSG_USER_BUDDYS : REDIS_MSG_BUDDYS, 0, _limit];
  var _logger 	  = this.logger;

  this.redis.zrange(_args, function (err, reply) {
    if (err) {
      _logger.warn(err);
      console.log(err);
      return cb(null);
    }
    cb('[' + reply + ']');
  });
};

method.begin = function ()
{
  this.multi = this.redis.multi();
};

method.commit = function (cb)
{
  var _logger 	= this.logger;

  this.multi.exec(function (err, reply) {
    if (err) {
      _logger.warn(err);
      console.log(err);
      cb(null);
    }
    cb(reply);
  });
};

method.save_msg = function (user_id, from_id, to_id, msg, stat, cb)
{
  //  var _timestamp = this.get_htime();
  try {
    var _stat 		= stat ? stat : 0;
    var pool_key 	= this.get_msg_pool_key(from_id, to_id);
    var _logger		= this.logger;
  } catch (e) {
    console.log(e);
    if (cb) {
      return  cb(false);
    }
    return false;
  }

  var _redis = this.redis;
  var _key 	= MQ_MSG_COUNTER;
  var _index    = from_id + '_' + to_id;

  var _stat_index = MQ_MSG_STAT_INDEX + '_'+ from_id + '_' + to_id;
  var _time_index = MQ_MSG_TIME_INDEX + '_'+ from_id + '_' + to_id;
  
  var _unread_to_key  = MQ_MSG_UNREAD_TO_COUNTER;
  var _unread_from_key  = MQ_MSG_UNREAD_FROM_COUNTER ;

  _redis.watch(_key);
  console.log('LLLLLL');
//  for (i = 0; i < 10; i++) {
    try {
      _redis.hget(_key, _index, function (reply) {
        console.log('...' + _key  + _index);
        console.log('** Save message get: ' + _key + ' ' + _index);
        var multi = _redis.multi();
        if (!reply) {
          reply = 1;
        } else {
          reply++;
        }
        multi.hset(_key, _index, reply);

        multi.exec(function (err, reply) {
          console.log('** 2Save message exec: ' + reply);
          var msg_str = JSON.stringify({id: reply, timestamp: Date.now(), body: msg, stat: _stat});
          try {
            /* Store msg body */
            _redis.hset(pool_key, reply, msg_str);
            _args = [_stat_index, _stat, reply];
            _redis.zadd(_args);
            _args = [_time_index, Date.now(), reply];
            _redis.zadd(_args);
          } catch(e) {
            console.log('error:' + e);
            _logger.warn(e);
            if (cb) {
              return cb(false);
            }
            return false;
          }

          if (stat != this.MQ_MSG_STAT_READED) {
            try {
              _redis.hincrby(_unread_from_key, to_id, 1);
              _redis.hincrby(_unread_to_key, from_id, 1);
            } catch (e) {
              console.log('** Save Message unread counter inc faile: ' + e);
              if (cb) {
                return cb(false);
              }
              return false;
            }
          }

          console.log('** 5Save message Exec: ');
        });

        return cb(reply);
      });
    } catch (e) {
 //     break;
      console.log('!! Get msg hash key error ' + e);
    }
  console.log('!! Get msg hash key error ' );
  var multi = _redis.multi();
  multi.discard();
  return cb(false);
};

method.get_my_unread_notify = function(user_id, cb)
{
  var _unread_key  = MQ_MSG_UNREAD_TO_COUNTER + '_' + user_id;

  if (!user_id) {
    cb(false);
  }
  var _logger = this.logger;

  this.redis.hgetall(_unread_key, function (err, reply) {
    if (err) {
      console.log('error:' + err);
      _logger.warn(err);
      cb(false);
    }
    cb(reply);
  });
};

method.set_readed = function(user_id, from_id, cb)
{
  var _unread_from_key  = MQ_MSG_UNREAD_FROM_COUNTER + '_' + from_id;
  var _unread_to_key = MQ_MSG_UNREAD_TO_COUNTER + '_' + user_id;
  this.redis.hdel(_unread_from_key, user_id);
  this.redis.hdel(_unread_to_key, from_id);
  cb(true);
};

method.get_my_sended_notify = function(user_id, cb)
{
  var _unread_key  = MQ_MSG_UNREAD_FROM_COUNTER + '_' + user_id;

  if (!user_id) {
    cb(false);
  }
  var _logger = this.logger;

  this.redis.hgetall(_unread_key, function (err, reply) {
    if (err) {
      console.log('error:' + err);
      _logger.warn(err);
      cb(false);
    }
    cb(reply);
  });
};

method.get_msg = function (from_id, to_id, limit, cb)
{
  if (!from_id || !to_id) {
    return cb(null);
  }

  var _limit 	= limit ? limit : 20;
  var pool_key 	= this.get_msg_pool_key(from_id, to_id);
  //  var _args		= [pool_key, '-' + String(_limit), '-1', 'WITHSCORES'];
  var _logger 	= this.logger;
  this.redis.hlen(pool_key, function (reply) {
    if (!reply) {
      return cb(null);
    }
    lazy.range(reply, reply - limit).join(function (xs) {
      this.redis.hmget(pool_key, xs, function (reply) {
        return cb(reply);
      });
    });
  });

  /*
     this.redis.zrange(_args, function (err, reply) {
     if (err) {
     console.log(err);
     _logger.warn(err);
     return cb(null);
     }
     if (cb) {
     return cb(reply);
     }
     });
     */
};

method.get_msg_size = function (from_id, to_id, cb)
{
  if (!from_id || !to_id) {
    return cb(null);
  }

  var pool_key 	= this.get_msg_pool_key(from_id, to_id);
  var _logger 	= this.logger;
  var _redis 	= this.redis;

  _redis.hlen(pool_key, function(reply) {
    console.log('heln:' + reply);
    return cb(reply);
  });
};

method.process_msg = function (client_id, topic, msg, stat, cb)
{
  console.log('in process_msg: ' + topic);
  var _ta = topic.split('.');

  console.log('_ta: ' + _ta + ': ' + client_id);
  if (!_ta) {
    console.log('!@_ta');
    return cb(false);
  }

  if (_ta[0] == '_S') {
    if (!_ta[1] || !_ta[2]) {
      console.log('!@_ta2');
      return cb(false);
    }

    this.save_msg(client_id, _ta[1], client_id,  msg, stat, function(msgid) {
      if (!msgid) {
        console.log('!! save msg error');
        cb(false);
      }
      if (cb) {
        cb({from_id: _ta[2], msg_id: msgid});
      }
    });
  }
};

module.exports = mq_msg;

/*
   var redis = require('./redis.js');
   var logger = require('./log.js')();

   mr = new redis([
   {host: "localhost", port: 6379, master: 1}],
   logger);

   var msg = new mq_msg(false, mr, logger, function (err, reply) {
   console.log(err);
   });

   _args = [REDIS_MSG_BUDDYS, 0, -1, 'WITHSCORES'];

   msg.get_buddys(false, function (reply) {
   console.log(reply);
   });

   console.log('i:2');

   msg.save_msg(1, 2, 'hello', 1, function(err) {
   msg.save_msg(1, 2, 'hello', 2);
   });

   msg.get_msg(1, 2, false, function (reply) {
   console.log(reply);
   });
   msg.get_msg_size(1, 2, function (reply) {
   console.log(reply);
   });
   */
