require('sys');
var winston = require('winston');

var date = new Date();
var now = date.getFullYear() + date.getDate();
var def_file_log_path = '/tmp/mqtt-server-log-' + now + '.log'

var method = mq_log.prototype;

function mq_log(transport, path, use_timestamp)
{
  this.transport = transport;

  if (!path)
    path = def_file_log_path;

  this.use_timestamp = use_timestamp ? true : false;

  switch (transport) {
    default:
      this.logger = new (winston.Logger)({
	transports: [
	new winston.transports.File({ filename:  path})
	],
	exceptionHandlers: [
	new winston.transports.File({ filename: def_file_log_path})
	]
      });
  }
}

method.get_transport = function()
{
  return this.transport;
}

method.warn = function(msg)
{
  var _msg = msg;

  if (this.use_timestamp) {
    date = new Date();
    _msg = date + _msg;
  }

  this.logger.warn(_msg);
}

module.exports = mq_log;

/*
mq_log = new mq_log('file');
console.log(mq_log.transport);
mq_log.logger.warn('hallo');
*/
